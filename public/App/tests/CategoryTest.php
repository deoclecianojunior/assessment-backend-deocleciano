<?php
require '/var/www/api/public/vendor/autoload.php';

use \GuzzleHttp\Client;
use \GuzzleHttp\Handler\MockHandler;
use \GuzzleHttp\HandlerStack;
use \GuzzleHttp\Psr7\Response;
use \GuzzleHttp\Psr7\Request;
use \GuzzleHttp\Exception\RequestException;

/**
* Class CategoryTest.
*/
class CategoryTest extends \PHPUnit\Framework\TestCase {

    protected $request_category;

    public function setUp() {
        $this->request_category = new GuzzleHttp\Client([
            'base_uri' => 'http://nginx',
            'headers' => []
        ]);
    }

    public function test_case_get_all_category(){
        $response = $this->request_category->get('/category', []);
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(), true);
    }

    public function test_case_get_one_category_invalid(){
        $response = $this->request_category->post(
            '/category/create', [GuzzleHttp\RequestOptions::MULTIPART, ['name' => 'Name', 'code' => 'Code']]
        );
        $this->assertEquals(200, $response->getStatusCode());
    }
}
