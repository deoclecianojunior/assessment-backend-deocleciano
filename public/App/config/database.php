<?php

use Illuminate\Database\Capsule\Manager as Db;

$db = new Db;

$db->addConnection([
  'host' => 'db',
  'username' => 'root',
  'password' => 'root',
  'database' => 'webjump',
  'driver' => 'mysql',
  'charset' => 'utf8',
  'collation' => 'utf8_unicode_ci',
  'prefix' => '',
  'port' => 3306
]);
$db->setAsGlobal();
$db->bootEloquent();