<?php
namespace App\Controllers\Product;

use Core\BaseController;
use App\Models\Product;
use App\Models\ProductCategory;

/**
* Controller Base, Contains the low level controller rule.
*
*
* @package   WebJump Challenge
* @category  Core System
* @author    Deocleciano Júnior
*/
class CreateController extends BaseController
{

    function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $data = $this->input->post();
        $product = Product::create($data);
        foreach ($data['categories'] as $key => $category) {
            $productCategory = array('category_id' => $category, 'product_id' => $product->id);
            ProductCategory::create($productCategory);
        }
        $this->redirect('/product');
    }
}
