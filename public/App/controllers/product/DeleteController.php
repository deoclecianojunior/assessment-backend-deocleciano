<?php
namespace App\Controllers\Product;

use Core\BaseController;
use App\Models\Product;
use App\Models\ProductCategory;
/**
* Controller Base, Contains the low level controller rule.
*
*
* @package   WebJump Challenge
* @category  Core System
* @author    Deocleciano Júnior
*/
class DeleteController extends BaseController
{

    private $id;

    function __construct($id)
    {
        parent::__construct();
        $this->id = $id;
    }

    public function run()
    {
        ProductCategory::where('product_id', $this->id)->delete();
        Product::find($this->id)->delete();
        $this->redirect('/product');
    }
}
