<?php
namespace App\Controllers\Product;

use Core\BaseController;
use App\Models\Category;

/**
* Controller Base, Contains the low level controller rule.
*
*
* @package   WebJump Challenge
* @category  Core System
* @author    Deocleciano Júnior
*/
class NewController extends BaseController
{

    function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $categories = Category::all();
        $this->view->render('product/new.html', ['categories' => $categories]);
    }
}
