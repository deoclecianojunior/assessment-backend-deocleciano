<?php
namespace App\Controllers\Category;

use Core\BaseController;
use App\Models\Category;
/**
* Controller Base, Contains the low level controller rule.
*
*
* @package   WebJump Challenge
* @category  Core System
* @author    Deocleciano Júnior
*/
class NewController extends BaseController
{

    function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $this->view->render('category/new.html');
    }
}
