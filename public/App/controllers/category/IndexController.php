<?php
namespace App\Controllers\Category;

use Core\BaseController;
use App\Models\Category;
/**
* Controller Base, Contains the low level controller rule.
*
*
* @package   WebJump Challenge
* @category  Core System
* @author    Deocleciano Júnior
*/
class IndexController extends BaseController
{

    function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $categories = Category::all();
        $this->view->render('category/index.html', ['categories' => $categories->toArray()]);
    }
}
