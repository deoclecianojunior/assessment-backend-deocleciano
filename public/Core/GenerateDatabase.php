<?php
namespace Core;

use Illuminate\Database\Capsule\Manager as Db;

class GenerateDatabase{

    public function runCategory (){
        Db::schema()->create('category', function ($table) {
            $table->increments('id');
            $table->string('name', 200);
            $table->string('code', 200);
            $table->timestamps();
        });
    }

    public function runProduct (){
        Db::schema()->create('product', function ($table) {
            $table->increments('id');
            $table->string('name', 200);
            $table->string('code', 200);
            $table->decimal('price', 8, 2);
            $table->longText('description');
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    public function runCategoryProduct(){
        Db::schema()->create('product_category', function ($table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();;
            $table->integer('product_id')->unsigned();;
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('category');

            $table->foreign('product_id')->references('id')->on('product');
        });
    }
}
