<?php
namespace Core;

use App\Models\Product;
use App\Models\Category;
use App\Models\ProductCategory;

class ImportCsv{

    private $path_files = '/var/www/api/public/import_files/';

    private $file_name;

    private $file_csv;

    public function __construct($file_name){
        $this->file_name = $file_name;
    }

    private function openFile(){
        $this->file_csv = fopen($this->path_files.$this->file_name, "r");
    }

    public function import(){
        print 'Iniciando Migração';
        $this->openFile();
        $i = 0;
        while (($column = fgetcsv($this->file_csv, 10000, ";")) !== FALSE) {
            $i++;
            if($i == 1){
                continue;
            }
            $categories = $this->getCategory($column[5]);
            $this->addProduct($column, $categories);
        }
        print 'Importação Finalizada com sucesso';
    }

    public function addProduct($dataLineFile, $categories){
        $productAdd = [
            'name' => $dataLineFile[0],
            'code' => $dataLineFile[1],
            'price' => $dataLineFile[4],
            'description' => $dataLineFile[2],
            'quantity' => $dataLineFile[3]
        ];
        $product = Product::create($productAdd)->toArray();
        $this->addProductCategory($product, $categories);
    }

    public function addProductCategory($product, $categories){
        foreach ($categories as $key => $category) {
            ProductCategory::create(['category_id' => $category['id'], 'product_id' => $product['id']]);
        }
    }

    public function getCategory($categoryDescription){
        $categories = explode('|',$categoryDescription);
        $categoryReturns = [];
        foreach ($categories as $key => $c) {
            $category = Category::where('name', $c)->get();
            if($category->isEmpty()){
                $categoryReturns[] = $this->addCategory($c);
            }else{
                $categoryReturns[] = $category->first()->toArray();
            }
        }
        return $categoryReturns;
    }
    public function addCategory($categoryDescription){
        return Category::create(['name' => $categoryDescription, 'code' => '9999'])->toArray();
    }
}
