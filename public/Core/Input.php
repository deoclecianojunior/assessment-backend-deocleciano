<?php

namespace Core;

class Input {
  
  public function post($param = false)
  {
    if ($param) {
      return (isset($_POST[$param])) ? $_POST[$param] : null; 
    }else{
      return $_POST;
    }
  }
}
