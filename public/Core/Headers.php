<?php
namespace Core;

class Headers 
{
  
  public static function setHeader($header, $value = false)
  {
    if (is_array($header)) {
      foreach ($header as $key => $value) {
        header("{$key}: {$value}");
      }
    } else {
      header("{$header}: {$value}");
    }
  }
}
