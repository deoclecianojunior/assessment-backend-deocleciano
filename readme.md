# Desafio WEBJUMP, Vaga Desenvolvedor Back-end | Deocleciano

# Algumas considrerações
- Procurei realizar o máximo de atividades dentro do prazo.
- Tive alguns problemas com o tempo, então eu não explorei muito a parte de testes por exemplo. Mas coloquei o recurso de teste e a cobertura de teste, funcionais.
- Utilizei Docker, então a documentação para rodar está toda baseada nele.

# Pre-requisitos
    - Ter o docker e o docker-compose instalado na máquina

# Instruções
- Clone o projeto
- Navegue até a pasta clonada e rode os comandos:
    1 - Para dar build nos container docker
        -> docker-compose up -d --build

    2 - Com o docker rodando, construa o banco de dados
        -> docker exec -it webjump-challenge_php_1 php public/webjump database

    3 - Rodar o composer para baixar as dependencias
        -> docker exec -it webjump-challenge_php_1 php composer-install.php

    4 - Agora já pode rodas a importação do CSV
        -> docker exec -it webjump-challenge_php_1 php public/webjump import


# Bibliotecas Utilizadas

- Rotas - Gerenciamento das rotas, achei em um tutorial que falava sobre o assunto, achei simples e funcional para pequenos projetos ou até mesmo para microserviços
    - https://github.com/phpzm/like-a-boss-3

- Eloquent ORM - Abstração de banco de dados, Orm bems imples, consigo extrair tudo do banco com pouca implementação, é bem completa também.
    - https://laravel.com/docs/5.0/eloquent

- PHPUnit - Testes automatizados, a principal ferramenta para testes unitários disponível para PHP.
    - https://phpunit.readthedocs.io/pt_BR/latest/

- Guzzle - ClienteHTTP PHP para auxiliar PHPUnit nas requisições
    - http://docs.guzzlephp.org/en/stable/

- PHP code coverage - Ferramenta gráfica para mostrar a cobertura de testes de testes unitários com PHPUnit
    - https://github.com/sebastianbergmann/php-code-coverage

- Twig - Template engine para montar views de forma rápida, flexível e organizada.
    - https://github.com/twigphp/Twig
